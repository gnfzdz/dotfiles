.DEFAULT_GOAL := help

BASEDIR := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
OS := $(shell uname -o)

PLUGIN_DIR := "${BASEDIR}/meta/plugins"

CUSTOM_PLUGIN_DIR := "${PLUGIN_DIR}/custom"
CONDITIONAL_PLUGIN_DIR := "${PLUGIN_DIR}/dotbot-conditional"

FLATPAK_PLUGIN := "${PLUGIN_DIR}/dotbot-flatpak/flatpak.py"
GIT_PLUGIN :="${PLUGIN_DIR}/dotbot-git/git.py"
SYNC_PLUGIN := "${PLUGIN_DIR}/dotbot-sync/sync.py"
INCLUDE_PLUGIN := "${PLUGIN_DIR}/dotbot-include/include.py"
GITCRYPT_PLUGIN := "${PLUGIN_DIR}/dotbot-gitcrypt/gitcrypt.py"
PLUGIN_ARGS := --plugin-dir "${CONDITIONAL_PLUGIN_DIR}" --plugin-dir "${CUSTOM_PLUGIN_DIR}" -p "${FLATPAK_PLUGIN}" -p "${GIT_PLUGIN}" -p "${SYNC_PLUGIN}" -p "${INCLUDE_PLUGIN}" -p "${GITCRYPT_PLUGIN}"

DOTBOT_DIR := "meta/dotbot"
DOTBOT_BIN := "bin/dotbot"

DOTBOT_CMD := "${BASEDIR}/meta/dotbot/bin/dotbot"

DOTBOT_BASE_CMD := ${DOTBOT_CMD} -v -d ${BASEDIR}

GCRYPT_KEY := ${BASEDIR}/.git/git-crypt/keys/default

EXECUTABLES := git git-crypt python ssh
ifeq (Android,${OS})
EXECUTABLES := ${EXECUTABLES} okc-gpg
endif
MISSING := $(strip $(foreach exec,$(EXECUTABLES),$(if $(shell which $(exec) 2>/dev/null),,$(exec))))

HAS_FLATPAK := $(if $(shell command -v flatpak 2>/dev/null),yes,no)

dependencies:
ifneq (,${MISSING})
ifeq (Android,${OS})
	@pkg install git git-crypt python openssh okc-agents
else
	$(error '${MISSING}' not found in PATH. Please install from package manager)
endif
endif
.PHONY: dependencies

submodules:
	@git submodule update --init --recursive
.PHONY: submodules

init: dependencies submodules
.PHONY: init

$(GCRYPT_KEY):
ifeq (Android,${OS})
	@okc-gpg -d .git-crypt/keys/default/0/772FB346CE4445C11910DCEF70270A9BF3BAFC88.gpg -o temp_keyfile
	@git-crypt unlock temp_keyfile
	@rm temp_keyfile
else
	@git-crypt unlock
endif

unlock: $(GCRYPT_KEY)
.PHONY: unlock

lock:
	@git-crypt lock
.PHONY: lock

bootstrap:
	@$(DOTBOT_BASE_CMD) -c meta/config/bootstrap.conf.yaml $(PLUGIN_ARGS)
.PHONY: bootstrap

flatpak:
	@$(DOTBOT_BASE_CMD) -c meta/config/flatpak.conf.yaml $(PLUGIN_ARGS)
.PHONY: flatpak

personal:
	@$(DOTBOT_BASE_CMD) -c meta/config/personal.conf.yaml $(PLUGIN_ARGS)
.PHONY: personal

public:
	@$(DOTBOT_BASE_CMD) -c meta/config/public.conf.yaml $(PLUGIN_ARGS)
.PHONY: public

workspace:
	@$(DOTBOT_BASE_CMD) -c meta/config/workspace.conf.yaml $(PLUGIN_ARGS)
.PHONY: workspace

help: ## Show this help message
	@echo ''
	@echo 'Usage:'
	@echo '  make <target>'
	@echo ''
	@echo 'Targets:'
	@echo '  help           Show this help message'
	@echo '  init           Initialize the dotfile repository for usage'
	@echo '  lock           Lock the repository using git-crypt'
	@echo '  unlock         Unlock the repository using git-crypt'
	@echo ''

	@echo '  bootstrap      Apply minimal personal configuration'
	@echo '  flatpak        Install/update a selection of flatpak applications'
	@echo '  personal       Apply personal configuration (gpg, ssh and personal workspace)'
	@echo '  public         Apply all public configurations'
	@echo '  workspace      Create/configure project specific workspaces'

.PHONY: help
# end
