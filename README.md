## Dotfiles

This repository contains my personal dotfiles for reference. Dotfiles are managed using [dotbot](https://github.com/anishathalye/dotbot) with useful commands in the Makefile.

### Setup

1. Install dependencies

```sh

# Arch Linux
sudo pacman -Syu python make git git-crypt gnupg emacs fd ripgrep

# Termux
pkg install python make git git-crypt gnupg emacs fd ripgrep okc-agents

```

2. Clone and initialize the repository
```
git clone https://gitlab.com/gnfzdz/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
make init
```

3. Apply public configuration
```sh
make public
```

4. Apply personal configuration
```sh
# Steps involving encrypted files will be skipped if the repository is still locked
make private

# The repository can be locked and the full configuration applied with the below commands
make unlock
make private
```
