#!/usr/bin/env python
# -*- coding: utf-8 -*-

import dotbot
import subprocess
from dotbot.dispatcher import Dispatcher

class Pkg(dotbot.Plugin):
    '''
    Install Termux packages using the pkg command
    '''

    _directive = 'pkg'

    def can_handle(self, directive):
        return directive == self._directive

    def handle(self, directive, data):
        if not self.can_handle(directive):
            raise ValueError('Pkg cannot handle directive ' % directive)
        return self._install_packages(data)

    def _install_packages(self, data):
        command = ['pkg','-y']
        for p in data:
            command.append(p)
        result = subprocess.run(command)
        return result.returncode == 0