from dotbot_conditional import Condition

import dotbot.util
import sys

class TermuxCondition(Condition):

    """
    Condition testing if currently running within Termux
    """

    _directive = "termux"

    def can_handle(self, directive):
        return directive == self._directive

    def handle(self, directive, expected=True):
        if not self.can_handle(directive):
            raise ValueError("%s cannot handle directive %s" % (self.__name, directive))

        ret = dotbot.util.shell_command('command -v termux-setup-storage', cwd=self._context.base_directory())
        return (ret == 0) == expected
